package com.legacy.step.player.util;

import net.minecraft.world.entity.player.Player;
import net.minecraft.nbt.CompoundTag;

public interface IStepPlayer
{
	CompoundTag writeAdditional(CompoundTag nbt);

	void read(CompoundTag nbt);

	boolean hasResetStepHeight();

	void resetStepHeight(boolean updated);

	Player getPlayer();
	
	void tick();
}