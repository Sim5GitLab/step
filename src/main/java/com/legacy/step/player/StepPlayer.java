package com.legacy.step.player;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.step.StepRegistry;
import com.legacy.step.player.util.IStepPlayer;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.NonNullSupplier;

public class StepPlayer implements IStepPlayer
{
	public static final Capability<IStepPlayer> INSTANCE = CapabilityManager.get(new CapabilityToken<>()
	{
	});

	private Player player;
	private boolean hasResetStepHeight = true;

	public StepPlayer()
	{
	}

	public StepPlayer(Player player)
	{
		super();
		this.player = player;
	}

	@Nullable
	public static IStepPlayer get(Player player)
	{
		return StepPlayer.getIfPresent(player, (skyPlayer) -> skyPlayer);
	}

	public static <E extends Player> void ifPresent(E player, Consumer<IStepPlayer> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			action.accept(player.getCapability(INSTANCE).resolve().get());
	}

	@Nullable
	public static <E extends Player, R> R getIfPresent(E player, Function<IStepPlayer, R> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return null;
	}

	public static <E extends Player, R> R getIfPresent(E player, Function<IStepPlayer, R> action, NonNullSupplier<R> elseSupplier)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return elseSupplier.get();
	}

	@Override
	public CompoundTag writeAdditional(CompoundTag compound)
	{
		compound.putBoolean("ResetStepEnchHeight", this.hasResetStepHeight());

		return compound;
	}

	@Override
	public void read(CompoundTag compound)
	{
		this.resetStepHeight(compound.getBoolean("ResetStepEnchHeight"));
	}

	@Override
	public Player getPlayer()
	{
		return this.player;
	}

	@Override
	public boolean hasResetStepHeight()
	{
		return this.hasResetStepHeight;
	}

	@Override
	public void resetStepHeight(boolean update)
	{
		this.hasResetStepHeight = update;
	}

	@Override
	public void tick()
	{
		Player entity = this.getPlayer();

		float enchantmentLevel = EnchantmentHelper.getEnchantmentLevel(StepRegistry.STEPPING, entity);
		float defaultStepHeight = 0.6F;

		if (enchantmentLevel > 0 && entity.maxUpStep < enchantmentLevel)
		{
			entity.maxUpStep = enchantmentLevel + 0.1F;

			this.resetStepHeight(false);
		}
		else if (entity.maxUpStep > defaultStepHeight && enchantmentLevel <= 0)
		{
			if (!hasResetStepHeight)
			{
				entity.maxUpStep = defaultStepHeight;
				this.resetStepHeight(true);
			}
		}
	}
}
