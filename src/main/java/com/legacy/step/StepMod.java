package com.legacy.step;

import com.legacy.step.player.util.IStepPlayer;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(StepMod.MODID)
public class StepMod
{
	public static final String MODID = "step";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public StepMod()
	{
		MinecraftForge.EVENT_BUS.addListener(StepMod::onCapsRegistered);
		MinecraftForge.EVENT_BUS.register(StepEntityEvents.class);
	}

	public static void onCapsRegistered(final RegisterCapabilitiesEvent event)
	{
		event.register(IStepPlayer.class);
	}
}