package com.legacy.step;

import com.legacy.step.player.StepPlayer;
import com.legacy.step.player.util.CapabilityProvider;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class StepEntityEvents
{
	@SubscribeEvent
	public static void onCapabilityAttached(AttachCapabilitiesEvent<Entity> event)
	{
		if (event.getObject() instanceof Player && !event.getObject().getCapability(StepPlayer.INSTANCE).isPresent())
			event.addCapability(StepMod.locate("player_capability"), new CapabilityProvider(new StepPlayer((Player) event.getObject())));
	}

	@SubscribeEvent
	public static void onLivingUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof Player)
			StepPlayer.ifPresent((Player) event.getEntity(), stepPlayer -> stepPlayer.tick());
	}
}
